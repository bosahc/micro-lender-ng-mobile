import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoanApplyPage } from '../pages/loan-apply/loan-apply';
import { RegisterPage } from '../pages/register/register';
import { LoginPage } from '../pages/login/login';
import { LoanHistoryPage } from '../pages/loan-history/loan-history';

@Component({
  templateUrl: 'app.html'
})
export class MicroLenderNGApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any, icon: string}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();
    this.pages = [
      { title: 'Home', component: HomePage , icon: 'home'},
      { title: 'Apply for Loan', component: LoanApplyPage , icon: 'clipboard'},
      { title: 'Register', component: RegisterPage , icon: 'person-add'},
      { title: 'Log In', component: LoginPage , icon: 'key'}
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    if (typeof page === 'string'){
      if (page=='loan-history'){
        this.nav.setRoot(LoanHistoryPage);
      }
    }else{
      this.nav.setRoot(page.component);
    }
  }

  openTutorial() {
    //this.nav.setRoot('TutorialPage');
  }

  // we want the active page to be highlighted whether on tabs or in the menu
  isActive(page: any) {
    // let childNav = this.nav.getActiveChildNav();

    // // Tabs are a special case because they have their own navigation
    // if (childNav) {
    //   if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
    //     return 'primary';
    //   }
    //   return;
    // }

    // if (this.nav.getActive() && this.nav.getActive().name === page.name) {
    //   return 'primary';
    // }
    // return;
  }

}
