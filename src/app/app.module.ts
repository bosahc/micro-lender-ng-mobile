import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { HttpModule } from '@angular/http';

import { MicroLenderNGApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ContactPage } from '../pages/contact/contact';
import { RegisterPage } from '../pages/register/register';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { LoanApplyPage } from '../pages/loan-apply/loan-apply';
import { LoanTermsPage } from '../pages/loan-terms/loan-terms';
import { LoanFinalPage } from '../pages/loan-final/loan-final';
import { LoanViewPage } from '../pages/loan-view/loan-view';
import { LoanHistoryPage } from '../pages/loan-history/loan-history';
import { LoanAirtimePage } from '../pages/loan-airtime/loan-airtime';
// import { SocialSharing } from '@ionic-native/social-sharing';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';


//Removed ....

@NgModule({
  declarations: [
    MicroLenderNGApp,
    HomePage,
    ContactPage,
    RegisterPage,
    LoginPage,
    ProfilePage,
    LoanApplyPage,
    LoanTermsPage,
    LoanFinalPage,
    LoanViewPage,
    LoanHistoryPage,
    LoanAirtimePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MicroLenderNGApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MicroLenderNGApp,
    HomePage,
    ContactPage,
    RegisterPage,
    LoginPage,
    ProfilePage,
    LoanApplyPage,
    LoanTermsPage,
    LoanFinalPage,
    LoanViewPage,
    LoanHistoryPage,
    LoanAirtimePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SocialSharing,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]
})
export class AppModule {}
