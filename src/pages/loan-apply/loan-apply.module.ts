import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoanApplyPage } from './loan-apply';

@NgModule({
  declarations: [
    LoanApplyPage,
  ],
  imports: [
    IonicPageModule.forChild(LoanApplyPage),
  ],
})
export class LoanApplyPageModule {}
