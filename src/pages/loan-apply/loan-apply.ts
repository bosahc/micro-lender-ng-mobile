import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoanTermsPage } from '../loan-terms/loan-terms';


@IonicPage()
@Component({
  selector: 'page-loan-apply',
  templateUrl: 'loan-apply.html',
})
export class LoanApplyPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoanApplyPage');
  }

  moveToTerms(){
    this.navCtrl.push(LoanTermsPage);
  }
}
