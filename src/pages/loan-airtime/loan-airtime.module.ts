import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoanAirtimePage } from './loan-airtime';

@NgModule({
  declarations: [
    LoanAirtimePage,
  ],
  imports: [
    IonicPageModule.forChild(LoanAirtimePage),
  ],
})
export class LoanAirtimePageModule {}
