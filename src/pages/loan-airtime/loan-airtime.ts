import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoanFinalPage } from '../loan-final/loan-final';


@IonicPage()
@Component({
  selector: 'page-loan-airtime',
  templateUrl: 'loan-airtime.html',
})
export class LoanAirtimePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }


  moveToFinal(){
    this.navCtrl.push(LoanFinalPage);
  }
}
