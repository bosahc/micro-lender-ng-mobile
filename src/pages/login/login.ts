import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, AlertController, LoadingController } from 'ionic-angular';
import { NgForm } from '@angular/forms';



@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loading: Loading;
  login: {username?: string, password?: string} = {};
  submitted = false;
  public type ='password';
  public showPass = false;


  constructor(private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private navCtrl: NavController,
    private navParams: NavParams,) {

    }


  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  toggleShowPassword() {
    this.showPass =!this.showPass;

    if(this.showPass) {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }


  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {

      this.showLoading();
      // this.sbs.login(this.login.username, this.login.password).then((allowed:boolean) => {
      //   if (allowed){
      //     setTimeout(()=>{
      //       this.loading.dismiss();
      //       this.navCtrl.setRoot('TabsPage');
      //     });
      //   }else{
      //     this.showError("Access Denied - Invalid Username or Password.");
      //   }
      // }, (error:any)=>{
      //   this.loading.dismiss();
      //   this.showError(error);
      // });

    }
  }

  public showLoading(){
    this.loading = this.loadingCtrl.create({
      content: 'Please wait ....'
    });
    this.loading.present;
  }

  public showError(text:string){
    setTimeout(()=>{
      this.loading.dismiss();
    });
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }

  onSignup() {
    this.navCtrl.push('RegisterPage');
  }

}
