import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoanViewPage } from '../loan-view/loan-view';


@IonicPage()
@Component({
  selector: 'page-loan-history',
  templateUrl: 'loan-history.html',
})
export class LoanHistoryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoanHistoryPage');
  }

  moveToLoanView(){
    this.navCtrl.push(LoanViewPage);
  }

  moveToPayment(){
    this.navCtrl.push(LoanViewPage);
  }
}
