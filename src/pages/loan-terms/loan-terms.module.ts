import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoanTermsPage } from './loan-terms';

@NgModule({
  declarations: [
    LoanTermsPage,
  ],
  imports: [
    IonicPageModule.forChild(LoanTermsPage),
  ],
})
export class LoanTermsPageModule {}
