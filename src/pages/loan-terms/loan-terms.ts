import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoanFinalPage } from '../loan-final/loan-final';


@IonicPage()
@Component({
  selector: 'page-loan-terms',
  templateUrl: 'loan-terms.html',
})
export class LoanTermsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoanTermsPage');
  }

  moveToFinal(){
    this.navCtrl.push(LoanFinalPage);
  }

}
