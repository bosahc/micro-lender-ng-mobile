import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, ModalOptions } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  user: any;
  investor: any;
  bankList:any;
  brokerList: any;
  userBroker: any ={};
  submitted = false;
  public type ='password';
  public showPass = false;
  public barLabel: string = "Password strength:";


  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  registerForm: FormGroup;

  constructor(private alertCtrl: AlertController,
              private modalCtrl: ModalController,
              private navCtrl: NavController,
              private navParams: NavParams,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(){
    this.initializeForm();

    // //Get banks list
    // this.sbs.getBanks().subscribe(res => {
    //   this.bankList = res.response;
    //   console.log(this.bankList)
    // });

  }

  private initializeForm() {

    let email = null;
    let phone = null;
    let bvn = null;
    let account_num = null;
    let bank = null;

    // We build our form using FormBuilder and include validators
    this.registerForm = this.formBuilder.group({
      // lastname: [lastname, Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*')])],
      email: [email, Validators.compose([Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),Validators.required])],
      phone: [phone, Validators.compose([Validators.required,Validators.pattern('.{11,11}')])],
      passwords: this.formBuilder.group({
        password: ["", Validators.compose([Validators.required, Validators.pattern('.{6,25}')])],
        confirmPassword: [null, Validators.required]}, {validator: this.checkPasswords
      }),
      bvn: [bvn, Validators.compose([Validators.required, Validators.pattern('.{11,11}')])],
      account_num: [account_num, Validators.compose([Validators.required, Validators.pattern('.{10,10}')])],
      bank: [bank, Validators.required],
      // broker: [broker, Validators.required]
    });
  }

  checkPasswords(group: FormGroup) {
      let pass = group.controls.password.value;
      let confirmPass = group.controls.confirmPassword.value;
      return pass === confirmPass ? null : { notSame: true }
    }


  toggleShowPassword() {
    this.showPass =!this.showPass;

    if(this.showPass) {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }


  onSignup() {
    this.submitted = true;

    if (this.registerForm.valid) {

    //   const signupValue = Object.assign({}, this.registerForm.value);
    //   signupValue.password = signupValue.passwords.password;
    //   delete signupValue.passwords;
    //   console.log(signupValue)

    //   //validate account number, bvn, email, phone number


    //     this.sbs.registerInvestor(signupValue).then((result:string) => {
    //       if (result !== "success"){
    //         //display the error message
    //         if (result === null) {
    //           this.showError("An error has occurred. Please try again.");
    //         }else{
    //           this.showError(result);
    //         }
    //       }else{
    //         //The sign up was successful and forward to login page
    //         this.navCtrl.push('LoginPage',{
    //           message: "Your registration was successful"
    //         });
    //       }
    //     });

    }


  }

  public showError(text:string){

    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }




}
