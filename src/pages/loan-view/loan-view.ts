import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-loan-view',
  templateUrl: 'loan-view.html',
})
export class LoanViewPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  moveToPayment(){

  }

}
