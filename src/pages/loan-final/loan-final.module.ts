import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoanFinalPage } from './loan-final';

@NgModule({
  declarations: [
    LoanFinalPage,
  ],
  imports: [
    IonicPageModule.forChild(LoanFinalPage),
  ],
})
export class LoanFinalPageModule {}
