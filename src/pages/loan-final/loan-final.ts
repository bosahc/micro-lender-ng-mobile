import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { SocialSharing } from '@ionic-native/social-sharing';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-loan-final',
  templateUrl: 'loan-final.html',
})
export class LoanFinalPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public socialSharing: SocialSharing) {
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad LoanFinalPage');
  }


  compilemsg(index):string{
    var msg = "";
    return msg.concat(" \n Sent from my Awesome App !");
  }

  regularShare(index){
    var msg = this.compilemsg(index);
    this.socialSharing.share(msg, null, null, null);
  }

  whatsappShare(index){
    var msg  = this.compilemsg(index);
    this.socialSharing.shareViaWhatsApp(msg, null, null);
   }

  twitterShare(index){
    var msg  = this.compilemsg(index);
    this.socialSharing.shareViaTwitter(msg, null, null);
  }

  facebookShare(index){
    var msg  = this.compilemsg(index);
    this.socialSharing.shareViaFacebook(msg, null, null);
  }

  moveToHome(){
    this.navCtrl.setRoot(HomePage);
  }

}
