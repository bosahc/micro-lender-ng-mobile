import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoanApplyPage } from '../loan-apply/loan-apply';
import { LoanAirtimePage } from '../loan-airtime/loan-airtime';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {



  constructor(public navCtrl: NavController) {
  }

  applyForLoan(){
    this.navCtrl.push(LoanApplyPage);
  }

  applyForAirtime(){
    this.navCtrl.push(LoanAirtimePage);
  }
}
